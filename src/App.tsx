import { useTranslation } from 'react-i18next';
import './App.scss';
import './Assets/Styles/_partials/mixins.scss';
import './Assets/Styles/_partials/colors.scss';
import './Assets/Styles/_partials/home.scss';
import Home from './Pages/Home';

function App() {
  const { t } = useTranslation('common');
  return (
    <section className='App'>
      <nav className='nav'>
          <h1>{t('home.nav.title')}</h1>
      </nav>
      <main className='main'>
        <Home />
      </main>
      <footer>
          <p>{t('footer.text')}</p>
      </footer>
    </section>
  );
}

export default App;
