import { Line } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from 'chart.js';
import { Dataset, MetricsData } from "../Entities/types";
import { filterElementsByKey } from "../Helper/Object.helper";

/**
 * Graph Component
 * 
 * @param props {metrics: object}
 * @returns Graph component
 */
const Graph = (props: {metrics: Object}) => {
    // Setup chart
    ChartJS.register(...registerables);
    // parse data
    const parsedData = filterElementsByKey(props.metrics);
    // Get labels - Use set to remove duplicates
    const labels = [...new Set(parsedData.map(([, data]: [string, MetricsData]) => data.times).flat())];
    // Create DataSets
    const datasets = parsedData.map(([key, data]: [string, MetricsData]) => new Dataset(key, data));
    // Create data object
    const data = {
        labels: labels,
        datasets: datasets
    };
    return(
        <Line 
            options={{
                responsive: true
            }}
            data={data}
        />
    )
};

export default Graph;