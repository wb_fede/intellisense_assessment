import { CircularProgress } from "@mui/material"

const Loader = () => {
    return (
        <div className="full-page-loader">
            <CircularProgress />
        </div>
    )
}

export default Loader;