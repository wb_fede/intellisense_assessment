import { MetricsData } from '../Entities/types';
import { useTranslation } from 'react-i18next';
import { filterElementsByKey } from '../Helper/Object.helper';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { DEFAULT_PAGE_SIZE } from '../Helper/Constants';
import { pick } from 'lodash';

/**
 * BasicTable component
 * 
 * @param props {metrics: Object}
 * @returns BasicTable component
 */
const BasicTable = (props: {metrics: Object, updateSelectedMetrics: Function}) => {
  const updateSelectedMetrics = props.updateSelectedMetrics;
  const { t } = useTranslation('common');
  const metrics = props.metrics;
  const rows = parseData(metrics).map(([key, value]) => {
    return {
      id: key,
      value: value
    }
  });
  // define columns
  const columns: GridColDef[] = [{
    field: 'id',
    headerName: t('home.table.metric'),
    flex: 1.5
  },{
    field: 'value',
    headerName: t('home.table.value'),
    flex: 0.5
  }]
  return (
    <DataGrid
      rows={rows}
      columns={columns}
      pageSize={DEFAULT_PAGE_SIZE}
      checkboxSelection
      onSelectionModelChange={(newSelectionModel) => {
        updateSelectedMetrics(pick(metrics, newSelectionModel));
      }}
    />
  );
};

/**
 * Parse response data
 * 
 * @param TK1Object TK1 object
 * @returns Array of [key, data]
 */
const parseData = (TK1Object: Object) => {
    // filter by key
    return filterElementsByKey(TK1Object)
          // map last value
          .map(([key, data]: [string, MetricsData]) => [key, (data.values || [])[data.values.length - 1]]);
}

export default BasicTable;