export type MetricsData = {
    times: string[];
    values: number[];
}

export class Dataset{
    label: string;
    data: number[] | string[];
    borderColor: string;
    backgroundColor: string;

    constructor(key: string, data: MetricsData){
        this.label = key;
        this.data = data.values;
        this.borderColor = this.backgroundColor = this.getRandomColor();
    }

    /**
     * Generate random color
     * 
     * @returns RGB string color
     */
    getRandomColor(){
        const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52);
        return `rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`;
    }
}