export const API_URL = 'https://reference.intellisense.io/thickenernn/v1/referencia';

export const TK1Regex = new RegExp(/^TK1_.*/);

export const DEFAULT_PAGE_SIZE = 5;