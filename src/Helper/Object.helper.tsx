import { TK1Regex } from "./Constants";

/**
 * Returns only TK1 elements
 * 
 * @param metrics Metrics object
 * @returns Array of [key, data]
 */
export const filterElementsByKey = (metrics: Object) => {
    return Object.entries(metrics)
            .filter(([key,]) => TK1Regex.test(key));
}