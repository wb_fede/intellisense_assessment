import { ToggleButton, ToggleButtonGroup } from '@mui/material';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Graph from '../Components/Graph';
import Loader from '../Components/Loader';
import BasicTable from '../Components/Table';
import { API_URL } from '../Helper/Constants';

/**
 * Home page
 * 
 * @returns Home page
 */
const Home = () => {
    const { t } = useTranslation('common');
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [metricsTK1, setCurrentMetricsTK1] = useState([]);
    const [alignment, setAlignment] = useState('table');
    const [selectedTK1Metrics, setSelectedMetrics] = useState({});

    const handleChange = (
        event: React.MouseEvent<HTMLElement>,
        newAlignment: string,
    ) => {
        if(!newAlignment) return;
        setAlignment(newAlignment);
    };

    useEffect(() => {
        fetch(API_URL)
            .then(res => res.json())
            .then(
                (response) => {
                    setCurrentMetricsTK1(response?.current?.data?.TK1);
                    setIsLoaded(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, []);
    if (error) {
        return <div  className='home-section'>Error: {(error as any).message}</div>;
    } else if (!isLoaded) {
        return <Loader />;
    } else {
        const disabledGraph = !Object.keys(selectedTK1Metrics).length;
        return (
            <section className='home-section'>
                <div className='actions'>
                <ToggleButtonGroup
                    color="primary"
                    value={alignment}
                    exclusive
                    onChange={handleChange}
                    >
                    <ToggleButton value="table" data-testid="table-toggle-button">{t('home.actions.table')}</ToggleButton>
                    <ToggleButton value="graph" data-testid="graph-toggle-button" disabled={disabledGraph}>{t('home.actions.graph')}</ToggleButton>
                </ToggleButtonGroup>
                </div>
                <div className='metrics-wrapper'>
                    <div className={alignment !== 'table' ? 'hidden' : 'block'}>
                        <BasicTable 
                        metrics={metricsTK1} 
                        updateSelectedMetrics={setSelectedMetrics.bind(this)}
                        data-testid="basic-table-component"/>
                    </div>
                    <div className={alignment !== 'graph' ? 'hidden' : 'block'}>
                        <Graph metrics={selectedTK1Metrics} 
                        data-testid="graph-table-component"/>
                    </div>
                </div>
            </section>
        );
    }
} 

export default Home;