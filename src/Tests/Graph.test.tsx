import { act } from "react-dom/test-utils";
import { render} from '@testing-library/react';
import Graph from '../Components/Graph';

// Mocks
jest.mock("react-i18next", () => ({
    useTranslation: () => ({ t: (key: string) => key }),
}));

jest.mock('chart.js', () => ({
    Chart:{
        register: () => {}
    },
    registerables: []
}));

const mockLineComponent = jest.fn();
jest.mock("react-chartjs-2", () => ({
    Line: (props: Object) => {
        mockLineComponent(props);
        return <div data-testid="chart"/>
    }
}))


const mockMetrics = {
    "Data_Quality_Status_TK1_Feed_Percent_Solids": {
        "times": [
            0
        ],
        "values": [
            0
        ]
    },
    "TK1_Underflow_Flow_Rate": {
        "times": [
            0,
            5,
            15,
            25,
            35,
            45,
            55
        ],
        "values": [
            4.3618499506,
            4.5584127903,
            4.9952191353,
            5.2354626656,
            5.2354626656,
            5.2354626656,
            5.2354626656
        ],
        "direct": 4.5584127903,
        "min": null,
        "max": null
        }
}

describe('Graph component', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });
    test('It should render graph', async() => {
        let component: any;
        await act(async () => {
            component = render(<Graph metrics={mockMetrics}/>);
        });
        // it should show table
        expect(component.getByTestId('chart')).toBeInTheDocument();
    });

    test('It should parse data', async() => {
        let component: any;
        await act(async () => {
            component = render(<Graph metrics={mockMetrics}/>);
        });
        // it should show table
        const mockCall = mockLineComponent.mock.calls[0];
        expect(mockCall[0].data.labels).toStrictEqual([0, 5, 15, 25, 35, 45, 55]);
        expect(mockCall[0].data.datasets[0].data).toStrictEqual([4.3618499506, 4.5584127903, 4.9952191353, 5.2354626656, 5.2354626656, 5.2354626656, 5.2354626656]);
    });
});
