import TestRenderer from 'react-test-renderer';
import Home from '../Pages/Home';
import { act } from "react-dom/test-utils";
import { render, screen, waitFor } from '@testing-library/react';

// Mocks
const mockGraphComponent = jest.fn();
jest.mock("../Components/Graph", () => (props: Object) => {
    mockGraphComponent(props);
    return <div data-testid="graph-component"/>
})

const mockTableComponent = jest.fn();
jest.mock("../Components/Table", () => (props: Object) => {
    mockTableComponent(props);
    return <div data-testid="basic-table-component"/>
})
jest.mock("react-i18next", () => ({
    useTranslation: () => ({ t: (key: string) => key }),
}));

const mockResponse = {
    current:{
        data:{
            TK1:{
                "Data_Quality_Status_TK1_Feed_Percent_Solids": {
                    "times": [
                        0
                    ],
                    "values": [
                        0
                    ]
                },
                "TK1_Underflow_Flow_Rate": {
                    "times": [
                        0,
                        5,
                        15,
                        25,
                        35,
                        45,
                        55
                    ],
                    "values": [
                        4.3618499506,
                        4.5584127903,
                        4.9952191353,
                        5.2354626656,
                        5.2354626656,
                        5.2354626656,
                        5.2354626656
                    ],
                    "direct": 4.5584127903,
                    "min": null,
                    "max": null
                    },
            }
        }
    }
}

describe('Home component', () => {
    beforeEach(() => {
        jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(mockResponse)
        } as any);
    });
    afterEach(() => {
        jest.restoreAllMocks();
    });
    test('It should show loader until data is received', async() => {
        const wrapper = TestRenderer.create(<Home />);
        const wrapperJSON = wrapper.toJSON() as any;
        expect(wrapperJSON.props.className).toBe('full-page-loader');
    });

    test('After data is received, it should render graph and table', async() => {
        let component: any;
        await act(async () => {
            component = TestRenderer.create(<Home />);
        });
        const wrapperJSON = component.toJSON() as any;
        expect(wrapperJSON.props.className).toBe('home-section');
        expect(wrapperJSON.children[1].props.className).toBe('metrics-wrapper');
    });

    test('By default, it should show table', async() => {
        let component: any;
        await act(async () => {
            component = render(<Home />);
        });
        // it should show table
        expect(component.getByTestId('basic-table-component')).toBeInTheDocument();
    });

    test('If user clicks on change view, it should show graph', async() => {
        let component: any;
        await act(async () => {
            component = render(<Home />);
        });
        // simulate click
        component.getByTestId('graph-toggle-button').click();
        
        // it should show table
        expect(component.getByTestId('basic-table-component')).toBeInTheDocument();
    });

    test('By default, graph toggle button should be disabled', async() => {
        let component: any;
        await act(async () => {
            component = render(<Home />);
        });
        // simulate click
        const graphToggleButton = component.getByTestId('graph-toggle-button');
        
        // it should show table
        expect(graphToggleButton).toHaveAttribute('disabled');
    })
});
