import { act } from "react-dom/test-utils";
import { render} from '@testing-library/react';
import Table from '../Components/Table';

// Mocks
jest.mock("react-i18next", () => ({
    useTranslation: () => ({ t: (key: string) => key }),
}));

const mockDataGrid = jest.fn();
jest.mock("@mui/x-data-grid", () => ({
    DataGrid: (props: Object) => {
        mockDataGrid(props);
        return <div data-testid="datagrid"/>
    },
    GridColDef: () => {}
}))


const mockMetrics = {
    "Data_Quality_Status_TK1_Feed_Percent_Solids": {
        "times": [
            0
        ],
        "values": [
            0
        ]
    },
    "TK1_Underflow_Flow_Rate": {
        "times": [
            0,
            5,
            15,
            25,
            35,
            45,
            55
        ],
        "values": [
            4.3618499506,
            4.5584127903,
            4.9952191353,
            5.2354626656,
            5.2354626656,
            5.2354626656,
            5.2354626656
        ],
        "direct": 4.5584127903,
        "min": null,
        "max": null
        },
        "TK1_Underflow_Flow_Rate2": {
            "times": [
                0,
                5,
                15,
                25,
                35,
                45,
                55
            ],
            "values": [
                4.3618499506,
                4.5584127903,
                4.9952191353,
                5.2354626656,
                5.2354626656,
                5.2354626656,
                5.2354626656
            ],
            "direct": 4.5584127903,
            "min": null,
            "max": null
            }
}

describe('Graph component', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });
    test('It should render datagrid', async() => {
        let component: any;
        await act(async () => {
            component = render(<Table metrics={mockMetrics} updateSelectedMetrics={() => {}}/>);
        });
        // it should show table
        expect(component.getByTestId('datagrid')).toBeInTheDocument();
    });
});