import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import i18next from 'i18next';
import { I18nextProvider, initReactI18next } from 'react-i18next';
import en from './Assets/i18n/en.json';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

// Translations
i18next
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    // the translations
    resources: {
      en: {
        common: en
      }
    },
    lng: "en",
    fallbackLng: "en",

    interpolation: {
      escapeValue: false
    }
});
root.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18next}>
      <App/>
    </I18nextProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
